FROM ubuntu:20.04
ENV TZ=UTC \
    DEBIAN_FRONTEND=noninteractive
USER root

RUN apt-get update
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa -y
RUN apt-get install -y python3.9
RUN ln -sf /usr/bin/python3.9 /usr/bin/python3
RUN apt-get install -y python3-pip
RUN pip3 install --upgrade pip
RUN python3 -m pip install --upgrade setuptools
# preparing directory
WORKDIR /app
ADD ./backend/. .
# delete env file if exists
RUN rm -f ./.env || true
ADD ./requirements.txt .

RUN mkdir -p /logs/django
RUN mkdir -p /logs/migrations


# install requirements
RUN apt install -y python3.9-dev default-libmysqlclient-dev build-essential libssl-dev libmysqlclient-dev
RUN pip3 install -r ./requirements.txt

EXPOSE 8080:8080

# CMD = ["python3", "manage.py", "runserver", "0.0.0.0:8080"]
CMD ["gunicorn", "-w", "4", "-k", "uvicorn.workers.UvicornWorker", "-b", "0.0.0.0:8080", "backend.asgi:application"]