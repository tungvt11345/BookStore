#!/bin/sh
list=$(docker volume ls -f name=logs | awk 'END { print NR }')
if [[ $list == 2 ]]; then
    echo "Docker volume /logs already exists. Continue ..."
else
    docker volume create logs
fi

{
    echo "Trying to remove container $container..."
    docker rm -f $container
    echo "You can ignore if its the error message. If no error occurs, then container has been deleted"
} || { 
    echo "Container $container was removed"
}
docker run -it --env-file=./container.env --name $container -h 127.0.0.1 -dp 8080:8000 --mount type=volume,source=logs,target=/logs $image:latest 