n=1
while [[ $n -le 5 ]]
do
    echo Checking if image $image can be run - $n
    # checking if container works
    if [[ -n "$(docker ps -aq -f name=$container)" ]]; then
        if [[ -n "$(docker ps -aq -f name=$container -f status=running)" ]]; then
            echo "Container run sucessful" 
        elif [[ -n "$(docker ps -aq -f name=$container -f status=exited)" ]]; then
            echo "Error: Container cannot be run"
            exit 1
        else
            echo "Container is not up yet"
        fi
    else
        echo "Could not find container"
    fi
    # sleeping for some seconds
    ((n++))
    sleep 5
done

if [[ ! -n "$(docker ps -aq -f name=$container)" ]]; then
    echo "Error: Timeout: Could not find container $container"
    exit 1
fi

echo "Removing container..."
docker rm -f $container